"""LCM type definitions
This file automatically generated by lcm.
DO NOT MODIFY BY HAND!!!!
"""

try:
    import cStringIO.StringIO as BytesIO
except ImportError:
    from io import BytesIO
import struct

class dynamixel_status_t(object):
    __slots__ = ["utime", "servo_id", "protocol", "error_flags", "control_mode", "position_radians", "speed", "load", "voltage", "temperature"]

    __typenames__ = ["int64_t", "int16_t", "int16_t", "int32_t", "int32_t", "double", "double", "double", "double", "double"]

    __dimensions__ = [None, None, None, None, None, None, None, None, None, None]

    ERROR_VOLTAGE = 1
    ERROR_ANGLE_LIMIT = 2
    ERROR_OVERHEAT = 4
    ERROR_OVERLOAD = 32
    MODE_WHEEL = 1
    MODE_JOINT = 2
    MODE_OTHER = 3

    def __init__(self):
        self.utime = 0
        self.servo_id = 0
        self.protocol = 0
        self.error_flags = 0
        self.control_mode = 0
        self.position_radians = 0.0
        self.speed = 0.0
        self.load = 0.0
        self.voltage = 0.0
        self.temperature = 0.0

    def encode(self):
        buf = BytesIO()
        buf.write(dynamixel_status_t._get_packed_fingerprint())
        self._encode_one(buf)
        return buf.getvalue()

    def _encode_one(self, buf):
        buf.write(struct.pack(">qhhiiddddd", self.utime, self.servo_id, self.protocol, self.error_flags, self.control_mode, self.position_radians, self.speed, self.load, self.voltage, self.temperature))

    def decode(data):
        if hasattr(data, 'read'):
            buf = data
        else:
            buf = BytesIO(data)
        if buf.read(8) != dynamixel_status_t._get_packed_fingerprint():
            raise ValueError("Decode error")
        return dynamixel_status_t._decode_one(buf)
    decode = staticmethod(decode)

    def _decode_one(buf):
        self = dynamixel_status_t()
        self.utime, self.servo_id, self.protocol, self.error_flags, self.control_mode, self.position_radians, self.speed, self.load, self.voltage, self.temperature = struct.unpack(">qhhiiddddd", buf.read(60))
        return self
    _decode_one = staticmethod(_decode_one)

    _hash = None
    def _get_hash_recursive(parents):
        if dynamixel_status_t in parents: return 0
        tmphash = (0x9f682c755a2274f1) & 0xffffffffffffffff
        tmphash  = (((tmphash<<1)&0xffffffffffffffff) + (tmphash>>63)) & 0xffffffffffffffff
        return tmphash
    _get_hash_recursive = staticmethod(_get_hash_recursive)
    _packed_fingerprint = None

    def _get_packed_fingerprint():
        if dynamixel_status_t._packed_fingerprint is None:
            dynamixel_status_t._packed_fingerprint = struct.pack(">Q", dynamixel_status_t._get_hash_recursive([]))
        return dynamixel_status_t._packed_fingerprint
    _get_packed_fingerprint = staticmethod(_get_packed_fingerprint)

