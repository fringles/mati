# -*- coding: utf-8 -*-
"""

@author: Maty
"""
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def CalculateRotation(matriz):
    """
    La funcion recibe una matriz de rotacion perteneciente a SO(3), por el 
    momento solo se asegura que el determinante sea igual a 1, debo implementar
    que se cumpla RR' = I pero aparecen problemas numericos. Como salida entrega 
    los angulos de Euler correspondientes.
    """
    if matriz[2,0] == 1:
        print("igual a +1")
        beta = -np.pi/2
        alpha = 0
        gamma = -np.arctan2(matriz[0,1], matriz[1,1])
    elif matriz[2,0] == -1:
        print("igual a -1")
        beta = np.pi/2
        alpha = 0
        gamma = np.arctan2(matriz[0,1], matriz[1,1])
    else:
        print("distinto de 1")
        beta = np.arctan2(-matriz[2,0], np.sqrt(matriz[0,0]**2 + matriz[1,0]**2))
        alpha = np.arctan2(matriz[1,0], matriz[0,0])
        gamma = np.arctan2(matriz[2,1], matriz[2,2])
    return gamma, beta, alpha
    
def Euler2Rot(alpha, beta, gamma):
    return np.array([[np.cos(alpha)*np.cos(beta), np.cos(alpha)*np.sin(beta)*np.sin(gamma) - np.sin(alpha)*np.cos(gamma), np.cos(alpha)*np.sin(beta)*np.cos(gamma) + np.sin(alpha)*np.sin(gamma)],
                     [np.sin(alpha)*np.cos(beta), np.sin(alpha)*np.sin(beta)*np.sin(gamma) + np.cos(alpha)*np.cos(gamma), np.sin(alpha)*np.sin(beta)*np.cos(gamma) - np.cos(alpha)*np.sin(gamma)],
                     [-np.sin(beta), np.cos(beta)*np.sin(gamma), np.cos(beta)*np.cos(gamma)]])
      
    
def axisPlotter(R): 
    #R = T[0:3, 0:3]

    omega_hat, theta = thetaFind(R)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.quiver(0,0,0,omega_hat[0], omega_hat[1], omega_hat[2])
    ax.axis([-5,5 , -5 , 5])
    ax.set_zlim3d(-5,5)
    ax.set_xlabel("$\hat{x}$")
    ax.set_ylabel("$\hat{y}$")
    plt.show()
            
    
        
        
def thetaFind(R):
    print(R - R.T)
    if (R.shape[0] == R.shape[1]) and np.allclose(R, np.eye(R.shape[0])):
        print(1)
        theta = 0
        omega_hat = None
    elif np.matrix.trace(R) == -1:
        print(2)
        theta = np.pi
        omega_hat = 1./(np.sqrt(2*(1 + R[2,2])))*np.array([[R[0,2], R[1,2], 1. + R[2,2]]]).T
    else:
        print(3)
        theta = np.arccos(0.5*(np.matrix.trace(R) - 1. ))
        omega_mat = 1./(2*np.sin(theta)) * (R - R.T)
        print(omega_mat)
        omega_hat = np.array([[omega_mat[2,1], omega_mat[0,2], omega_mat[1,0]]]).T
    return omega_hat, theta

def corchete(vector):
    return np.array([[0 , -vector[2], vector[1]],[vector[2], 0, -vector[0]],[-vector[1], vector[0], 0]])

def screwFind(T):
    R = T[0:3, 0:3]
    p = T[0:3,3].T
    if (R.shape[0] == R.shape[1]) and np.allclose(R, np.eye(R.shape[0])):
        omega = 0*p
        norma = np.linalg.norm(p)
        vel = p/norma
        theta = norma
    else:
        omega_hat, theta = thetaFind(R)
        omega_mat = corchete(omega_hat)
        pass
        
    
        
        
def Rx(theta):
    return np.array([[1, 0, 0],[0, np.cos(theta), -np.sin(theta)],[0 , np.sin(theta), np.cos(theta)]])

def Ry(theta):
    return np.array([[np.cos(theta), 0, np.sin(theta)],[0, 1 , 0],[ -np.sin(theta), 0, np.cos(theta)]])

def Rz(theta):
    return np.array([[np.cos(theta), -np.sin(theta), 0],[ np.sin(theta),np.cos(theta), 0],[0, 0 , 1]])



     
if __name__ == "__main__":
    # Creamos las matrices de rotacion
    rx = Rx(np.pi/4)
    ry = Ry(-np.pi/4)
    rz = Rz(np.pi/6)
    
    rot = np.matmul(ry, rz)
    
    ## Con la matriz de rotacion calcularemos los angulos de euler
    gamma, beta, alpha = CalculateRotation(rx)
    print("La matriz de rotacion obtenida al utilizar \n alpha: {} ; beta : {} ; gama {} es \n {}".format(
            alpha, beta, gamma, Euler2Rot(alpha, beta, gamma)))
    
    
    
    print("La matriz de rotacion esta dada por \n {}".format(Euler2Rot(alpha, beta, gamma)))
    
    ## Graficaremos distintos ejes de rotacion, se ha elegido colocar este vector
    ## en el origen del sistema de coordenadas.
    
    
    R = np.matmul(rx, rz)

    print("La matriz de rotacion resultante esta dada por: \n{}".format(R))
    axisPlotter(R)
    