import modern_robotics as mr
import numpy as np

l1 = 0.04
l2 = 0.08
l3 = 0.08
l4 = 0.06



M = np.array([[-1, 0,  0, 0],
              [ 0, 1,  0, 6],
              [ 0, 0, -1, 2],
              [ 0, 0,  0, 1]])
# Slist tiene tantas filas como screws se definieron
Slist = np.array([[0, 0, 1,  0, 0, 0],
                  [0, 1, 0,  -(l1), 0, 0],
                  [0, 1, 0,  -(l1+l2), 0, 0],
		  [0, 1, 0,  -(l1+l2+l3), 0, 0]]).T
# Tantos thetas como articulaciones hay
thetalist = np.array([np.pi/2 , 3 , np.pi, 0 ])
# Retorna la matriz homogénea de transformación, representando el marco de referencia del gripper
# cuando las articulaciones están en esos ángulos
T0N = mr.FKinSpace(M, Slist, thetalist)
d = T0N[:,3][:3] # distancia en x,y,z desde {s}

print("x_(s) = {}".format(d))
